package org.travel;


import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("org.travel.mapper")
public class TravelApplication {
    public static void main(String[] args){
        SpringApplication.run(TravelApplication.class,args);
    }
}
