package org.travel.controller;


import org.springframework.web.bind.annotation.*;
import org.travel.domain.entity.UserEntity;
import org.travel.service.UserService;
import org.travel.util.RestResult;

import javax.annotation.Resource;

@RestController
@RequestMapping("user")
public class UserController {
    @Resource
    private UserService userService;

    @RequestMapping("hello")
    public String sayHello() {
        return userService.hello();
    }

    @PostMapping("login")
    public RestResult login(@RequestBody UserEntity userEntity) {
        return userService.login(userEntity);
    }

    @GetMapping("info")
    public RestResult info(@RequestParam("id") int id) {
        return userService.info(id);
    }

    @PostMapping("update")
    public RestResult update(@RequestBody UserEntity userEntity){
        return  userService.update(userEntity);
    }

    @PostMapping("register")
    public RestResult register(@RequestBody UserEntity userEntity) { return userService.register(userEntity); }

}
