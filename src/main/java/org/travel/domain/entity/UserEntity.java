package org.travel.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity {
    private int id;
    private String name;
    private String gender;
    private String phone;
    private String password;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    private int isDelete;
}
