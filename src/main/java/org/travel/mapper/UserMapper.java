package org.travel.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.travel.domain.entity.UserEntity;

@Mapper
public interface UserMapper {
    UserEntity selectUserinfoByUserId(int id);

    UserEntity selectUserByPhoneAndPassword(UserEntity userEntity);

    int updateUserById(UserEntity userEntity);

    int insetUser(UserEntity userEntity);
}
