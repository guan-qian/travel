package org.travel.service;

import org.travel.domain.entity.UserEntity;
import org.travel.util.RestResult;

public interface UserService {
    String hello();

    RestResult login(UserEntity userEntity);

    RestResult info(int id);

    RestResult update(UserEntity userEntity);

    RestResult register(UserEntity userEntity);
}
