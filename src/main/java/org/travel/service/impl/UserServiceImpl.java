package org.travel.service.impl;


import org.springframework.stereotype.Service;
import org.travel.domain.entity.UserEntity;
import org.travel.mapper.UserMapper;
import org.travel.service.UserService;
import org.travel.util.RestResult;
import org.travel.util.RestResultBuilder;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public String hello(){
        return userMapper.selectUserinfoByUserId(1).getName();
    }

    @Override
    public RestResult login(UserEntity userEntity) {
        UserEntity user = userMapper.selectUserByPhoneAndPassword(userEntity);
        if (user == null) {
            return new RestResultBuilder().fail();
        }
        return new RestResultBuilder().success(user.getId());
    }

    @Override
    public RestResult info(int id) {
        UserEntity userEntity = userMapper.selectUserinfoByUserId(id);

        if (userEntity == null) {
            return new RestResultBuilder().fail("用户不存在");
        }
        return new RestResultBuilder().success(userEntity);
    }

    @Override
    public RestResult update(UserEntity userEntity){
        int userCol = userMapper.updateUserById(userEntity);
        if(userCol == 0){
            return new RestResultBuilder().fail("修改失败");
        }
        return new RestResultBuilder().success();
    }

    @Override
    public  RestResult register(UserEntity userEntity){
        int userCol = userMapper.insetUser(userEntity);
        if(userCol == 0){
            return new RestResultBuilder().fail("修改失败");
        }
        return new RestResultBuilder().success();
    }

}
