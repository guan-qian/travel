package org.travel.util;

import java.util.HashMap;

public class RestResultBuilder<T> {
    private Integer code = 200;
    private String msg = "success";
    private T data;

    public RestResultBuilder<T> setCode(Integer code) {
        this.code = code;
        return this;
    }

    public RestResultBuilder<T> setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public RestResultBuilder<T> setData(T data) {
        this.data = data;
        return this;
    }

    public RestResult<T> build() {
        return new RestResult<T>(code, msg, data != null ? data : (T) new HashMap());
    }

    public RestResult<T> fail() {
        return new RestResult<T>(ResultCode.FAIL.getCode(), ResultCode.FAIL.getMsg(), (T) new HashMap());
    }

    public RestResult<T> fail(String msg) {
        return new RestResult<T>(ResultCode.FAIL.getCode(), msg, (T) new HashMap());
    }

    public RestResult<T> fail(ResultCode resultCode) {
        return new RestResult<T>(resultCode.getCode(), resultCode.getMsg(), (T) new HashMap());
    }

    public RestResult<T> success() {
        return new RestResult<T>(this.code, this.msg, (T) new HashMap());
    }

    public RestResult<T> success(String msg) {
        return new RestResult<T>(this.code, msg, (T) new HashMap());
    }

    public RestResult<T> success(T t) {
        return new RestResult<T>(this.code, this.msg, t);
    }
}
