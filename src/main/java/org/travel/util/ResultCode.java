package org.travel.util;

public enum ResultCode {
    SYSTEM_BUSY(-1, "system busy"),
    SUCCESS(200, "success"),
    FAIL(400, "fail"),
    ERROR_ACCOUNT_PASSWORD(401, "Incorrect account password"),
    REJECT_REQUEST(403, "reject request"),
    NOT_FOUND(404, "not found"),
    TOKEN_MISSION(405, "token mission"),
    TOKEN_OVERDUE(406, "token overdue"),
    TOKEN_INVALID(407, "token invalid"),
//    HAVE_NO_LEGAL_POWER(407, "Have no legal power"),
    USER_NOT_EXIST(409, "user not exist"),
    USERNAME_OR_PASSWORD_ERROR(410, "username or password error"),
    CLIENT_AUTHENTICATION_FAILED(411, "client authentication failed"),
    ACCESS_UNAUTHORIZED(412, "access unauthorized"),
    TOKEN_INVALID_OR_EXPIRED(413, "token invalid or expired"),
    TOKEN_ACCESS_FORBIDDEN(415, "token access forbidden"),
    FLOW_LIMITING(416, "flow limiting"),
    DEGRADATION(417, "degradation"),
    SERVICE_NO_AUTHORITY(418, "service no authority"),
    SERVER_INTERNAL_ERROR(500, "Server internal error"),
    SERVICE_IS_NOT_AVAILABLE(503, "service is not available"),
    ;

    private Integer code;
    private String msg;

    ResultCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
